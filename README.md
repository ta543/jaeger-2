# 🕵️ Jaeger Microservices Project

Welcome to the Jaeger Microservices Project! This repository demonstrates a microservices architecture integrated with Jaeger for tracing 📊. It includes three sample services, each containerized using Docker 🐳, orchestrated with Kubernetes, and deployment automated through GitLab CI/CD pipelines 🚀.

## 📑 Overview

The goal of this project is to showcase how Jaeger can be integrated into a microservices architecture to provide distributed tracing capabilities. This is essential for monitoring and troubleshooting complex distributed systems 🌐.

## 🏗️ Project Structure

The project is organized into several directories to maintain a clean and manageable structure:

- `src/`: Contains the source code for three microservices 🖥️.
- `k8s/`: Kubernetes YAML configurations for deploying the services and Jaeger 📦.
- `infra/`: Contains Terraform and Ansible scripts for setting up infrastructure 🔧.
- `tests/`: Unit and integration tests for the microservices 🧪.
- `docs/`: Additional documentation for setup and usage 📖.

## 🛠️ Technologies Used

This project utilizes a range of technologies, each serving a specific purpose:

- **Jaeger**: Provides distributed tracing to monitor and troubleshoot transactions 🕸️.
- **Docker**: Containerization of the microservices 🐳.
- **Kubernetes**: Orchestration of the containers ⚙️.
- **GitLab CI/CD**: Automates the build, test, and deployment process 🚀.
- **Terraform**: Infrastructure as code for provisioning resources 🌍.
- **Ansible**: Automation tool for configuration management ⚒️.
- **Python/Flask**: The microservices are written in Python using the Flask framework 🐍.
