from flask import Flask, request, jsonify
import logging
from jaeger_client import Config
import opentracing

app = Flask(__name__)

def init_tracer(service):
    logging.getLogger('').handlers = []
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    config = Config(
        config={
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            'logging': True,
            'local_agent': {
                'reporting_host': 'jaeger-agent',
                'reporting_port': '6831',
            },
        },
        service_name=service,
    )
    return config.initialize_tracer()

tracer = init_tracer('service2')

@app.route("/")
def home():
    with tracer.start_span('home-service2') as span:
        span.log_kv({'event': 'home-request', 'message': 'Hello, this is service2'})
        return "Hello from Service 2!"

@app.route("/api/process")
def process_data():
    with tracer.start_active_span('process-data') as scope:
        scope.span.log_kv({'event': 'process-request', 'value': 'processing data'})
        return jsonify({'result': 'Data processed by Service 2'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)
