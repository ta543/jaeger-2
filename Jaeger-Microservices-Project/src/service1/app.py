from flask import Flask, request, jsonify
import logging
from jaeger_client import Config
import opentracing

app = Flask(__name__)

def init_tracer(service):
    logging.getLogger('').handlers = []
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    config = Config(
        config={
            'sampler': {
                'type': 'const',
                'param': 1,
            },
            'logging': True,
            'local_agent': {
                'reporting_host': 'jaeger-agent',
                'reporting_port': '6831',
            },
        },
        service_name=service,
    )
    return config.initialize_tracer()

tracer = init_tracer('service1')

@app.route("/")
def home():
    with tracer.start_span('home') as span:
        span.log_kv({'event': 'home-request', 'message': 'Hello, this is service1'})
        return "Hello from Service 1!"

@app.route("/api/data")
def data():
    with tracer.start_active_span('fetch-data') as scope:
        scope.span.log_kv({'event': 'data-request', 'value': 'some data'})
        return jsonify({'data': 'Here is some data from Service 1'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
