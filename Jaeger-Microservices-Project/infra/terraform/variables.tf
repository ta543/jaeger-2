variable "region" {
  description = "The region where AWS resources will be created"
  default     = "us-east-1"
}
