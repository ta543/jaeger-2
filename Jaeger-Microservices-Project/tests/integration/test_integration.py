import unittest
import requests

class TestMicroservicesIntegration(unittest.TestCase):
    def test_microservice1_connection(self):
        """ Test microservice1 is up and responds correctly """
        response = requests.get('http://localhost:8080/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Hello from Service 1", response.text)

    def test_microservice2_connection(self):
        """ Test microservice2 is up and responds correctly """
        response = requests.get('http://localhost:8081/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Hello from Service 2", response.text)

    def test_microservice3_connection(self):
        """ Test microservice3 is up and responds correctly """
        response = requests.get('http://localhost:8082/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Hello from Service 3", response.text)

    def test_data_aggregation(self):
        """ Test data aggregation workflow """
        # Simulate data processing call to service2
        process_response = requests.get('http://localhost:8081/api/process')
        self.assertEqual(process_response.status_code, 200)
        self.assertIn("Data processed by Service 2", process_response.json()['result'])

        # Simulate data aggregation call to service3
        aggregate_response = requests.get('http://localhost:8082/api/aggregate')
        self.assertEqual(aggregate_response.status_code, 200)
        self.assertIn("Data aggregated by Service 3", aggregate_response.json()['result'])

if __name__ == '__main__':
    unittest.main()
