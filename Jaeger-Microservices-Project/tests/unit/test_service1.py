import unittest
from unittest.mock import patch
import app

class TestService1(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()
        self.app.testing = True

    @patch('app.tracer')
    def test_home(self, mock_tracer):
        """ Test the home endpoint of Service 1 """
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("Hello from Service 1", response.data.decode())

if __name__ == '__main__':
    unittest.main()
